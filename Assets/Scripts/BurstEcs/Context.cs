﻿using System;
using System.Threading;
using Unity.Collections;

namespace BurstEcs
{
	public struct Context : IDisposable
	{
		private int _id;
		private long _generationId;
		private NativeQueue<int> _freeId;

		private NativeArray<Entity> _entities;

		private int _createLocker;

		public Context(int entityCapacity) : this()
		{
			_id = 0;
			_generationId = 0;
			_createLocker = 0;
			_entities = new NativeArray<Entity>(entityCapacity, Allocator.Persistent);
			_freeId = new NativeQueue<int>(Allocator.Persistent);
		}

		public Entity CreateNew()
		{
			while (true)
			{
				if (_createLocker > 0)
					continue;

				if (Interlocked.Increment(ref _createLocker) == 1)
					break;
			}

			var id = _freeId.Count > 0 ? _freeId.Dequeue() : GetNextId();
			var entity = new Entity(id, _generationId++, 0);
			_entities[entity.Id] = entity;
			_createLocker = 0;
			return entity;
		}

		private int GetNextId()
		{
			if (_id >= _entities.Length)
			{
				var entities = new NativeArray<Entity>(_entities.Length << 1, Allocator.Persistent);
				NativeArray<Entity>.Copy(_entities, 0, entities, 0, _entities.Length);
				_entities.Dispose();
				_entities = entities;
			}

			return _id++;
		}

		public void Dispose()
		{
			_entities.Dispose();
			_freeId.Dispose();
		}
	}
}