﻿using System;
using Unity.Collections;

namespace BurstEcs
{
	public struct Entity : IDisposable
	{
		public int Id;
		public long GenerationId;

		private NativeBitArray _hasComponent;
		private NativeArray<int> _componentId;

		public Entity(int id, long generationId, int componentsCount) : this()
		{
			Id = id;
			GenerationId = generationId;
			_hasComponent = new NativeBitArray(componentsCount, Allocator.Persistent);
			_componentId = new NativeArray<int>(componentsCount, Allocator.Persistent);
		}

		public void Dispose()
		{
			_hasComponent.Dispose();
			_componentId.Dispose();
		}
	}
}